import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {AutenticacaoService} from '../../authentication/autenticacao.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @Output() exibirPainel: EventEmitter<string> = new EventEmitter();

  formulario: FormGroup = new FormGroup({
    email: new FormControl(null),
    senha: new FormControl(null)
  });

  constructor(
    private autenticacao: AutenticacaoService
  ) { }

  ngOnInit() {
  }

  exibirPainelCadstro() {
    this.exibirPainel.emit('cadastro');
  }

  autenticar() {
    console.log(this.formulario);
    this.autenticacao.autenticar(
      this.formulario.value.email,
      this.formulario.value.senha
    );
  }

}
