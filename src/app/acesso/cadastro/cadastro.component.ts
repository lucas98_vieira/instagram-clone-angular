import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {UsuarioModel} from '../../shared/model/usuario.model';
import {AutenticacaoService} from '../../authentication/autenticacao.service';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.scss']
})
export class CadastroComponent implements OnInit {
  @Output() exibirPainel: EventEmitter<string> = new EventEmitter();

  formulario: FormGroup = new FormGroup({
    email: new FormControl(null),
    nomeCompleto: new FormControl(null),
    nomeUsuario: new FormControl(null),
    senha: new FormControl(null)
  });
  constructor(
    private autenticacao: AutenticacaoService
  ) { }

  ngOnInit() {
  }

  exibirPainelLogin() {
    this.exibirPainel.emit('login');
  }

  cadastrarUsuario() {
    // console.log(this.formulario);
    const usuario: UsuarioModel = new UsuarioModel(
      this.formulario.value.email,
      this.formulario.value.nomeCompleto,
      this.formulario.value.nomeUsuario,
      this.formulario.value.senha
    );

    this.autenticacao.cadastrarUsuario(usuario)
      .then(() => this.exibirPainelLogin());
  }

}
