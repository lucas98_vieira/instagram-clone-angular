import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AcessoRoutingModule } from './acesso-routing.module';
import {AcessoComponent} from './acesso.component';
import {MaterialModule} from '../material.module';
import {BannerComponent} from './banner/banner.component';
import {CadastroComponent} from './cadastro/cadastro.component';
import {LoginComponent} from './login/login.component';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [AcessoComponent, BannerComponent, CadastroComponent, LoginComponent],
  imports: [
    CommonModule,
    AcessoRoutingModule,
    MaterialModule,
    ReactiveFormsModule
  ]
})
export class AcessoModule { }
