import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import {HomeComponent} from './home.component';
import {PublicacoesComponent} from './publicacoes/publicacoes.component';


@NgModule({
  declarations: [HomeComponent, PublicacoesComponent],
  imports: [
    CommonModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
