import { Component, OnInit } from '@angular/core';
import {AutenticacaoService} from '../authentication/autenticacao.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private autenticacao: AutenticacaoService) { }

  ngOnInit() {
  }

  sair() {
    this.autenticacao.sair();
  }

}
