import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';

import {AutenticacaoGuardService} from './shared/service/autenticacao-guard.service';
import {AcessoModule} from './acesso/acesso.module';

export const routes: Routes = [

  { path: 'acesso', loadChildren: () => import('./acesso/acesso.module').then(m => m.AcessoModule) },
  { path: 'home', loadChildren: () => import('./home/home.module').then(m => m.HomeModule), canActivate: [ AutenticacaoGuardService ] },
  {
    path: '',
    redirectTo: 'acesso',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
