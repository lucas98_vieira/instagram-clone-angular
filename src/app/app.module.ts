import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule} from '@angular/router';

import {AutenticacaoService} from './authentication/autenticacao.service';
import {AutenticacaoGuardService} from './shared/service/autenticacao-guard.service';

import { AppComponent } from './app.component';

import {AngularFireDatabaseModule} from '@angular/fire/database';
import {AngularFireModule} from '@angular/fire';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {AppRoutingModule, routes} from './app-routing.module';

export const firebaseConfig = {
  apiKey: 'AIzaSyC0JdDCcDinAgAldD2iBh0CmKuK9XQDGb8',
  authDomain: 'jta-instagram-clone-18fc6.firebaseapp.com',
  projectId: 'jta-instagram-clone-18fc6',
  storageBucket: 'jta-instagram-clone-18fc6.appspot.com',
  messagingSenderId: '881972990738',
  appId: '1:881972990738:web:0b36184f859f3809d759cb'
};


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFirestoreModule, // firestore
    AngularFireModule.initializeApp(firebaseConfig),
    AppRoutingModule
  ],
  providers: [AutenticacaoService, AutenticacaoGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
