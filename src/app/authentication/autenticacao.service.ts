import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import {UsuarioModel} from '../shared/model/usuario.model';
import {Injectable} from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {idTokenResult} from '@angular/fire/auth-guard';
import {Router} from '@angular/router';


@Injectable({
  providedIn: 'root'
})

export class AutenticacaoService {

  tokenId: string;
  constructor(
    public afAuth: AngularFireAuth,
    public afs: AngularFireDatabase,
    private router: Router
  ) {
  }


  cadastrarUsuario(usuario: UsuarioModel): Promise<any> {
    // console.log('Chegamos até o serviço', usuario);
    return this.afAuth.createUserWithEmailAndPassword(usuario.email, usuario.senha)
      .then((resposta: any) => {
        console.log(resposta);
        // remover a senha do atributo senha do objeto usuario
        delete usuario.senha;
        // console.log(usuario);
        // registrando dados complementares do usuario no path email na base64
        this.afs.list(`usuario_detalhe/${btoa(usuario.email)}`).push(usuario);
      }).catch((err: Error) => {
        console.log(err);
      });
  }

  autenticar(email: string, senha: string) {
    console.log(email, senha);
    this.afAuth.signInWithEmailAndPassword(email, senha)
      .then((resposta: any) => {
        this.afAuth.onIdTokenChanged(user => {
          if (user) {
            user.getIdToken(true).then(idToken => {
              this.tokenId = idToken;
              localStorage.setItem('idToken', idToken);
              this.router.navigate(['/home']);
            });
          }
        });
      })
      .catch((err: Error) => {
        console.log(err);
      });
  }

  autenticado() {

    if (this.tokenId === undefined && localStorage.getItem('idToken') !== null) {
      this.tokenId = localStorage.getItem('idToken');
    }

    if(this.tokenId === undefined ) {
      this.router.navigate(['/acesso']);
    }

    return this.tokenId !== undefined;
  }
  sair() {
    this.afAuth.signOut()
      .then(() => {
        localStorage.removeItem('idToken');
        this.tokenId = undefined;
        this.router.navigate(['/']);
      });
  }
}
